import { ref, computed } from "vue";
import { defineStore } from "pinia";
import { useUserStore } from "./user";
import { useMessageStore } from "./message";

export const useLoginStore = defineStore("login", () => {
  const useUser = useUserStore();
  const useMessage = useMessageStore();
  const loginName = ref("");
  const isLogin = computed(() => {
    return loginName.value !== "";
  });

  const login = (userName: string, password: string) => {
    if (useUser.login(userName, password)) {
      loginName.value = userName;
      localStorage.setItem("LoginName", userName);
    } else {
      useMessage.showMessage("Login failed ,Please try again");
    }
  };

  const logout = () => {
    loginName.value = "";
    localStorage.removeItem("LoginName");
  };

  const loadData = () => {
    loginName.value = localStorage.getItem("LoginName") || "";
  };

  return { loginName, isLogin, login, logout, loadData };
});
